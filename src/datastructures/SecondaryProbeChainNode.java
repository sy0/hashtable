package datastructures;

public class SecondaryProbeChainNode {

	private int index;
	private int j;
	private SecondaryProbeChainNode next;

	public SecondaryProbeChainNode(int index, int j) {
		this.index = index;
		this.j = j;
		next = null;
	}

	public SecondaryProbeChainNode getNext() {
		return next;
	}

	public void setNext(SecondaryProbeChainNode next) {
		this.next = next;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getJ() {
		return j;
	}

	public void setJ(int j) {
		this.j = j;
	}

}