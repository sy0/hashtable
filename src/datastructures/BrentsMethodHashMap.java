package datastructures;

import models.WordBucket;

import java.math.BigDecimal;

public class BrentsMethodHashMap<K, V> extends AbstractHashMap<K, V> {

	private MapEntry<K, V>[] table;

	public BrentsMethodHashMap(int cap, int p) {
		super(cap, p);
	}

	public BrentsMethodHashMap(int cap) {
		super(cap);
	}

	public BrentsMethodHashMap() {
		super();
	}

	@Override
	@SuppressWarnings("unchecked")
	protected void createTable() {
		table = (MapEntry<K, V>[]) new MapEntry[capacity];
	}

	@Override
	protected int hashValue(K key) {
		return (int) ((double) key % capacity);
	}

	protected int incrementFunction(K key) {
		double incrementD = ((double) key / capacity) % capacity;
		int increment = (int) incrementD;
		if (Math.round(incrementD) == 0) {
			// Because sometimes increments will be between 0 and 1.
			// And we don't want them to be 0.
			increment = (int) Math.ceil(incrementD);
		}
		return increment;
	}

	@Override
	protected V bucketGet(int h, K k) {
		int increment = incrementFunction(k);
		while (table[h] != null) {
			// Checking whether the keys are equal or not
			// Using BigDecimal class to be sure about equality of two double numbers
			double currentKey = (double) table[h].getKey();
			BigDecimal currentKeyBigDecimal = new BigDecimal(currentKey);
			BigDecimal keyBigDecimal = new BigDecimal((double) k);
			if (keyBigDecimal.compareTo(currentKeyBigDecimal) == 0) {
				((WordBucket) table[h].getValue()).setIndexWhenSearched(h);
				return table[h].getValue();
			}
			h = (h + increment) % capacity;
		}

		return null;
	}

	public V search(K key, String wordToSearch) {
		if (wordToSearch != null) {
			return get(key);
		}
		return null;
	}

	@Override
	protected V bucketPut(int h, K k, V v) {
		int increment = incrementFunction(k);
		int tempH = h;
		int oldH = h;

		for (int i = 0; i < 2; i++) {
			if (table[tempH] == null) {
				table[tempH] = new MapEntry<>(k, v);
				WordBucket entry = (WordBucket) table[tempH].getValue();
				entry.increaseCount();
				n++;
				return v;
			} else {
				WordBucket entry = (WordBucket) table[tempH].getValue();
				if (v.equals(entry)) {
					entry.increaseCount();
					return v;
				}
			}
			tempH = (tempH + increment) % capacity;
		}

		// If value exists already, just increase its count
		int countH = h;
		do {
			if (table[countH] == null) {
				break;
			}
			if (v.equals(table[countH].getValue())) {
				WordBucket entry = (WordBucket) table[countH].getValue();
				entry.increaseCount();
				return v;
			}
			countH = (countH + increment) % capacity;
		} while (countH != h);

		// Creating two-dimensional list to find the least costly process by Brent's Method
		MultiLinkedList brentsChain = new MultiLinkedList();
		int primaryCounter = 1;
		int secondaryCounter = 1;

		while (table[h] != null) {
			// Always checking if it's back to the beginning index
			// If it reaches the beginning index,
			// That means we can not place the word on the board
			if (brentsChain.containsPrimary(h)) {
				break;
			}

			PrimaryProbeChainNode currentChain = brentsChain.addPrimaryProbeChain(h, primaryCounter);
			primaryCounter++;

			if (table[h] != null) {
				int incrementOfSecondary = incrementFunction(table[h].getKey());
				int secondaryH = h;

				while (table[secondaryH] != null) {
					secondaryH = (secondaryH + incrementOfSecondary) % capacity;
					secondaryCounter++;
				}

				// Always checking if it's back to the beginning index
				// If it reaches the beginning index,
				// That means we can skip to next one for secondary chain
				if (brentsChain.containsSecondary(secondaryH)) {
					break;
				}

				brentsChain.addSecondaryProbeChain(currentChain, secondaryH, secondaryCounter);
			}

			secondaryCounter = 1;
			h = (h + increment) % capacity;
		}
		brentsChain.addPrimaryProbeChain(h, primaryCounter); // last node

		// Finding the shortest way
		int[] instructions = brentsChain.findShortestWay();
		if (instructions.length == 1) {
			// Only primary chain
			int newH = (oldH + increment * instructions[0]) % capacity;
			table[newH] = new MapEntry<>(k, v);
			WordBucket entry = (WordBucket) table[newH].getValue();
			entry.increaseCount();
			n++;
			return v;
		} else {
			// Both primary and secondary chain
			int newH = (oldH + increment * instructions[0]) % capacity;
			V secondaryValue = table[newH].getValue();
			K secondaryKey = table[newH].getKey();
			table[newH] = null;
			int incrementOfSecondary = incrementFunction(secondaryKey);
			int secondaryNewH = (newH + incrementOfSecondary * instructions[1]) % capacity;
			if (table[secondaryNewH] != null) {
				return null;
			}
			table[secondaryNewH] = new MapEntry<>(secondaryKey, secondaryValue);
			table[newH] = new MapEntry<>(k, v);
			WordBucket entry = (WordBucket) table[newH].getValue();
			entry.increaseCount();
			n++;
			return v;
		}
	}

	@Override
	protected V bucketRemove(int h, K k) {
		return null;
	}

	@Override
	public Iterable<Entry<K, V>> entrySet() {
		return null;
	}

}
