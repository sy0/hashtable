package datastructures;

public class PrimaryProbeChainNode {

	private int index;
	private int i;
	private PrimaryProbeChainNode down;
	private SecondaryProbeChainNode right;

	public PrimaryProbeChainNode(int index, int i) {
		this.index = index;
		this.i = i;
		down = null;
		right = null;
	}

	public PrimaryProbeChainNode getDown() {
		return down;
	}

	public void setDown(PrimaryProbeChainNode down) {
		this.down = down;
	}

	public SecondaryProbeChainNode getRight() {
		return right;
	}

	public void setRight(SecondaryProbeChainNode right) {
		this.right = right;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getI() {
		return i;
	}

	public void setI(int i) {
		this.i = i;
	}

}
