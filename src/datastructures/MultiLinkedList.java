package datastructures;

public class MultiLinkedList {

	private PrimaryProbeChainNode head;

	public MultiLinkedList() {
		head = null;
	}

	public PrimaryProbeChainNode addPrimaryProbeChain(int index, int i) {
		if (head == null) {
			head = new PrimaryProbeChainNode(index, i);
			return head;
		} else {
			PrimaryProbeChainNode temp = head;
			while (temp.getDown() != null) {
				temp = temp.getDown();
			}
			PrimaryProbeChainNode newNode = new PrimaryProbeChainNode(index, i);
			temp.setDown(newNode);
			return newNode;
		}
	}

	public void addSecondaryProbeChain(PrimaryProbeChainNode primaryProbeChainNodeToAdd, int index, int j) {
		if (head == null) {
			System.out.println("Add a primary probe chain before adding secondary probe chain!");
			return;
		}

		PrimaryProbeChainNode temp = head;
		while (temp != null) {
			if (primaryProbeChainNodeToAdd.equals(temp)) {
				SecondaryProbeChainNode temp2 = temp.getRight();
				if (temp2 == null) {
					SecondaryProbeChainNode newNode = new SecondaryProbeChainNode(index, j);
					temp.setRight(newNode);
				} else {
					while (temp2.getNext() != null) {
						temp2 = temp2.getNext();
					}
					SecondaryProbeChainNode newNode = new SecondaryProbeChainNode(index, j);
					temp2.setNext(newNode);
				}
			}
			temp = temp.getDown();
		}
	}

	public boolean containsPrimary(int index) {
		if (head == null) return false;

		PrimaryProbeChainNode temp = head;
		while (temp.getDown() != null) {
			if (temp.getIndex() == index) {
				return true;
			}
			temp = temp.getDown();
		}

		return false;
	}

	public boolean containsSecondary(int index) {
		if (head == null) return false;

		PrimaryProbeChainNode temp = head;
		while (temp.getDown() != null) {
			SecondaryProbeChainNode temp2 = temp.getRight();
			if (temp2 == null) {
				return false;
			} else {
				while (temp2.getNext() != null) {
					if (temp2.getIndex() == index) {
						return true;
					}
					temp2 = temp2.getNext();
				}
			}
			temp = temp.getDown();
		}

		return false;
	}

	public int[] findShortestWay() {
		if (head == null) return null;

		PrimaryProbeChainNode temp = head;
		int minI = 99;
		int minJ = 99;
		int minLength = 99;

		while (temp != null) {
			SecondaryProbeChainNode temp2 = temp.getRight();
			if (temp2 == null) {
				if (temp.getI() + 1 <= minLength) {
					minLength = temp.getI() + 1;
					minI = temp.getI();
					minJ = 0;
				}
			} else {
				while (temp2.getNext() != null) {
					temp2 = temp2.getNext();
				}
				if (temp.getI() + temp2.getJ() < minLength) {
					minLength = temp.getI() + temp2.getJ();
					minI = temp.getI();
					minJ = temp2.getJ();
				}
			}
			temp = temp.getDown();
		}
		
		if (minJ == 0) {
			return new int[]{(minI - 1)};
		} else {
			return new int[]{
				(minI - 1),
				(minJ - 1)
			};
		}
	}

}
