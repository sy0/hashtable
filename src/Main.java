import datastructures.BrentsMethodHashMap;
import helpers.ConsoleProgressBar;
import helpers.UniqueIdGenerator;
import models.WordBucket;

import java.io.Console;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		BrentsMethodHashMap<Double, WordBucket> b = new BrentsMethodHashMap<>(5003);
		ConsoleProgressBar consoleProgressBar = new ConsoleProgressBar();
		final String filePath = "story.txt";

		File file = new File(filePath);
		try {
			Scanner scanner = new Scanner(file);
			int wordCounter = 0;
			int percentage = 0;

			System.out.println("Reading the file, please wait...");
			while (scanner.hasNext()) {
				String word = scanner.next().trim();
				if (word != null) {
					Double id = UniqueIdGenerator.generate(word);
					if (id != -1) {
						WordBucket wordBucket = new WordBucket(id, word);
						b.put(id, wordBucket);
						wordCounter++;
						if (wordCounter >= 144) {
							percentage++;
							wordCounter = 0;
							consoleProgressBar.animate(percentage + "");
							Thread.sleep(50);
						}
					}
				}
			}
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}

		Scanner searchScanner = new Scanner(System.in);
		System.out.println("\nIt's done!");
		System.out.println("\n(write -exit if you want to close the program)");
		while (true) {
			System.out.print("\nWrite a word to search: ");
			String wordToSearch = searchScanner.nextLine().split(" ")[0]; // to ensure that only one word is taken

			if (wordToSearch.equalsIgnoreCase("-exit")) {
				System.exit(1);
			}

			Double keyToSearch = UniqueIdGenerator.generate(wordToSearch);
			if (keyToSearch != -1) {
				WordBucket wordBucket = b.search(keyToSearch, wordToSearch);
				if (wordBucket != null) {
					String word = wordBucket.getWord();
					double keyOfSearched = wordBucket.getKey();
					int count = wordBucket.getCount();
					int index = wordBucket.getIndexWhenSearched();
					System.out.println("Search: " + word);
					System.out.println("Key: " + keyOfSearched);
					System.out.println("Count: " + count);
					System.out.println("Index: " + index);
				} else {
					System.out.println("The text file does not contain the word you searched!");
				}
			} else {
				System.out.println("The text file does not contain the word you searched!");
			}
		}

	}
}
