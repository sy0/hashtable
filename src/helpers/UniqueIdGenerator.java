package helpers;

import exceptions.IncorrectCharacterException;

import java.util.Locale;

public class UniqueIdGenerator {

	private static final int ENGLISH_ALPHABET_LENGTH = 26;
	private static final int NUMBERS_LENGTH = 10;
	private static final int LETTERS_START = 65;
	private static final int LETTERS_END = 90;
	private static final int NUMBERS_START = 48;
	private static final int NUMBERS_END = 57;

	public static double generate(String word) {
		double id = 0;
		final double base = ENGLISH_ALPHABET_LENGTH + NUMBERS_LENGTH;

		word = word.toUpperCase(Locale.ENGLISH);

		try {
			int exponentOfBase = word.length() - 1;

			for (int i = 0; i < word.length(); i++) {
				char character = word.charAt(i);
				int characterCode = getCharacterCode(character);
				id += characterCode * Math.pow(base, exponentOfBase);
				exponentOfBase--;
			}
		} catch (IncorrectCharacterException incorrectCharacterException) {
			id = -1;
		}

		return id;
	}

	public static int getCharacterCode(char character) throws IncorrectCharacterException {
		int firstChar = (int) character;

		if (firstChar >= LETTERS_START && firstChar <= LETTERS_END) {
			return firstChar - 65;
		} else if (firstChar >= NUMBERS_START && firstChar <= NUMBERS_END) {
			return firstChar - 22;
		} else {
			throw new IncorrectCharacterException("A character which is unhandled by the generator is detected. It is: `" + character + "`");
		}
	}

}
