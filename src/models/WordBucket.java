package models;

import java.util.Locale;

public class WordBucket {

	private String word;
	private double key;
	private int count;
	private int indexWhenSearched;

	public WordBucket() {
		this("");
	}

	public WordBucket(String word) {
		this(0, word);
	}

	public WordBucket(double key, String word) {
		this.word = word;
		this.key = key;
		count = 0;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public double getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public void increaseCount() {
		count++;
	}

	public int getIndexWhenSearched() {
		return indexWhenSearched;
	}

	public void setIndexWhenSearched(int indexWhenSearched) {
		this.indexWhenSearched = indexWhenSearched;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;

		if (!(o instanceof WordBucket))
			return false;

		// typecast to compare words
		WordBucket c = (WordBucket) o;
		return this.getWord().toUpperCase(Locale.ENGLISH).equals(c.getWord().toUpperCase(Locale.ENGLISH));
	}

}