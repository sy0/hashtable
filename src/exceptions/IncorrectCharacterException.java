package exceptions;

public class IncorrectCharacterException extends Exception {

	public IncorrectCharacterException(String errorMessage) {
		super(errorMessage);
	}

	public IncorrectCharacterException(String errorMessage, Throwable throwable) {
		super(errorMessage, throwable);
	}

}